# m-surface

m-surface is a pedagogic toolkit to see and touch mathematical surfaces.

## trigonometric surfaces

1. $`z=\cos(x)\times \cos(y)`$

[![z=\cos(x)\times \cos(y)](images/a.png)](stl/cosx_cosy.stl)

2. $`z=\cos(x\times y)`$

[![z=\cos(x\times y)](images/b.png)](stl/cosxy.stl)

3. $`z=\cos(x^2 + y^2)`$

[![z=\cos(x^2 + y^2)](images/c.png)](stl/cosx2py2.stl)

4. $`z=\cos(x^2 - y^2)`$

[![z=\cos(x^2 - y^2)](images/d.png)](stl/cosx2my2.stl)

5. $`z=\cos\left(\frac{x}{1+ y^2}\right)`$

[![z=\cos\left(\frac{x}{1+ y^2}\right)](images/e.png)](stl/cosxd1py2.stl)





